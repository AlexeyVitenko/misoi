import math
import numpy

def sign(n):
    return math.copysign(1, n)

def rademacher(n,t):
    return sign(math.sin((2**n)*math.pi*t))

def frange(a, b, n):
    e = (b - a)/(n)
    return [a + x*e for x in range(0,n)]

def r_vector(n):
    return [rademacher(n, t) for t in frange(0.125, 1., 8)]

def wal(rad, r, n):
    return [reduce(lambda res,x: res*x ,row.flat) for row in numpy.matrix([([ra**((n >> (k-1) & 1) ^ (n >> (k) & 1)) for ra in rad[k - 1]]) for k in range(1,r + 1)]).transpose()]

r = [r_vector(n) for n in range(1,4)]
print r
print '>>>>>>>>>>>>>>>>'
p = 3
print [ wal(r, p, n) for n in range(0, 2**p)] 
